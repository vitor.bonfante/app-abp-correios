import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { URL } from "./api";

const apiAuth = axios.create({
  baseURL: URL,
  headers: {
    "Access-Control-Allow-Origin": "*",
    Accept: "*/*",
    "Content-Type": "application/json",
    "Accept-Encondig": "gzip, deflate, br",
  },
});

apiAuth.interceptors.request.use(
  async (req) => {
    const token = await AsyncStorage.getItem("access_token");

    //@ts-ignore
    req.headers.Authorization = `Bearer ${token}`;
    return req;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default apiAuth;

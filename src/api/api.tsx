import axios from "axios";

export const URL = "http://192.168.3.18:8080/api/client";

const apiPublic = axios.create({
  baseURL: URL,
  headers: {
    "Access-Control-Allow-Origin": "*",
    Accept: "*/*",
    "Content-Type": "application/json",
    "Accept-Encondig": "gzip, deflate, br",
  },
});

export default apiPublic;

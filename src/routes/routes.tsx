import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { TouchableOpacity } from "react-native-gesture-handler";
import LogoutHeaderButton from "../components/LogoutHeaderButton";

import HomeScreen from "../screens/Home";
import Login from "../screens/Login";
import ManagerHome from "../screens/ManagerHome";
import PostmanHome from "../screens/PostmanHome";
import TrackPackage from "../screens/TrackPackage";
import { RouteParamList } from "../utils/RouteParamList";

const Stack = createNativeStackNavigator<RouteParamList>();

export default function Routes() {
  async function logout(nav: any) {
    await AsyncStorage.clear();
    nav.navigate("HomeScreen");
  }

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="TrackPackage"
          component={TrackPackage}
          options={{ title: "Rastreio de encomenda" }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="PostmanHome"
          component={PostmanHome}
          options={({ navigation }) => ({
            title: "Entregador | Home",
            headerBackVisible: false,
            headerRight: () => (
              <LogoutHeaderButton onPress={() => logout(navigation)} />
            ),
          })}
        />
        <Stack.Screen
          name="ManagerHome"
          component={ManagerHome}
          options={({ navigation }) => ({
            title: "Gerente | Home",
            headerBackVisible: false,
            headerRight: () => (
              <LogoutHeaderButton onPress={() => logout(navigation)} />
            ),
          })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

import { useFocusEffect } from "@react-navigation/native";
import React from "react";
import { useState } from "react";
import { RefreshControl } from "react-native-gesture-handler";
import apiAuth from "../../api/apiAuth";
import NoRegisters from "../../components/NoRegisters";
import PackagesWaitingRoute from "../../components/PackagesWaitingRoute";
import { RouteStackParamList } from "../../utils/RouteParamList";
import ModalSelectRoute from "./ModalSelectRoute";
import { Container, FlatList, Scroll } from "./styles";

interface Package {
  rfid: string;
  id: string;
}

export default function ManagerHome({}: RouteStackParamList<"ManagerHome">) {
  const [packages, setPackages] = useState<Package[]>([]);
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [selectedId, setSelectedId] = useState<string>("");
  const [selectedRFID, setSelectedRFID] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);

  function openModal() {
    setModalOpen(true);
  }

  function closeModal() {
    setModalOpen(false);
    listPackages();
  }

  async function listPackages() {
    setLoading(true);
    await apiAuth
      .get("/package/list")
      .then((res) => {
        setLoading(false);
        setPackages(res.data);
      })
      .catch((err) => {
        setLoading(false);
        return alert(err.response.data.ServerMessage.message);
      });
  }

  useFocusEffect(
    React.useCallback(() => {
      listPackages();
    }, [])
  );

  return (
    <Container>
      <ModalSelectRoute
        isOpen={modalOpen}
        setModalClose={closeModal}
        selectedId={selectedId}
        selectedRFID={selectedRFID}
      />
      {packages.length > 0 ? (
        <FlatList
          data={packages}
          keyExtractor={(_, index) => index.toString()}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={listPackages} />
          }
          renderItem={({ item }: any) => {
            return (
              <PackagesWaitingRoute
                onPress={() => {
                  setSelectedId(item.id);
                  setSelectedRFID(item.rfid);
                  openModal();
                }}
                id={item.rfid}
              />
            );
          }}
        />
      ) : (
        <Scroll
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={listPackages} />
          }
        >
          <NoRegisters />
        </Scroll>
      )}
    </Container>
  );
}

import { useState } from "react";
import {
  Portal,
  Modal,
  Text,
  Button,
  TextInput,
  Avatar,
} from "react-native-paper";
import PickerSelect from "react-native-picker-select";
import DateTimePicker from "@react-native-community/datetimepicker";
import { ButtonTouchable, InputWrapper, RouteWrapper, Scroll } from "./styles";
import apiAuth from "../../../api/apiAuth";
import * as Clipboard from "expo-clipboard";
interface Props {
  isOpen: boolean;
  setModalClose: () => void;
  selectedId: string;
  selectedRFID: string;
}

interface RouteArray {
  unitiesId: string;
  estimateArriveDate: Date;
}

export default function ModalSelectRoute({
  isOpen,
  setModalClose,
  selectedId,
  selectedRFID,
}: Props) {
  const [routesArray, setRoutesArray] = useState<RouteArray[]>([]);

  const [weight, setWeight] = useState<string>("");
  const [invoice, setInvoice] = useState<string>("");
  const [orderId, setOrderId] = useState<string>("");
  const [name, setName] = useState<string>("");

  const [address, setAddress] = useState<string>("");
  const [city, setCity] = useState<string>("");
  const [state, setState] = useState<string>("");
  const [country, setCountry] = useState<string>("");
  const [neighborhood, setNeighborhood] = useState<string>("");
  const [number, setNumber] = useState<string>("");

  const unities = [
    {
      label: "Unidade Floripa",
      value: "d5b91098-a30f-45eb-b4d3-e70dc683ccf1",
    },
    {
      label: "Unidade de Itajaí",
      value: "8e3c0afe-29fe-4f1c-beea-b991369a2d5e",
    },
    {
      label: "Unidade de Tubarão",
      value: "475cd197-6b8b-4335-b5f9-b14a33b34d65",
    },
    {
      label: "Unidade de Criciúma",
      value: "92cbb409-5a27-443e-b94d-dcd70c011f530",
    },
  ];

  function addStep() {
    setRoutesArray((array) => [
      ...array,
      { estimateArriveDate: new Date(), unitiesId: "000" },
    ]);
  }

  function clear() {
    setRoutesArray([]);
    setWeight("");
    setInvoice("");
    setOrderId("");
    setName("");
    setAddress("");
    setCity("");
    setState("");
    setCountry("");
    setNeighborhood("");
    setNumber("");
  }

  async function saveInfos() {
    if (
      !selectedId ||
      !weight ||
      !invoice ||
      !orderId ||
      !name ||
      !address ||
      !city ||
      !state ||
      !country ||
      !neighborhood ||
      !number ||
      routesArray.length < 1
    ) {
      return alert("Preencha corretamente os campos!");
    }

    await apiAuth
      .put("/package/update", {
        packageId: selectedId,
        weight: Number(weight),
        invoice,
        orderId,
        address,
        city,
        country,
        neighborhood,
        number: Number(number),
        state,
        name,
        routesArray,
      })
      .then(async (res) => {
        await Clipboard.setStringAsync(selectedId);
        alert("Rota definida com sucesso!");
        alert(
          "O número de rastreio foi copiado para sua área de transferência"
        );
        setModalClose();
        clear();
      })
      .catch((err) => {
        return alert(err.response.data.ServerMessage.message);
      });
  }

  return (
    <Portal>
      <Modal
        visible={isOpen}
        contentContainerStyle={{ backgroundColor: "white", padding: 12 }}
        onDismiss={() => {
          clear();
          setModalClose();
        }}
        style={{ padding: 12 }}
      >
        <Scroll>
          <Text
            variant="titleLarge"
            style={{ alignSelf: "center", textAlign: "center" }}
          >
            Selecione a rota e as informações do pacote
          </Text>
          <Text
            variant="titleLarge"
            style={{
              alignSelf: "center",
              textAlign: "center",
              fontWeight: "bold",
            }}
          >
            {selectedRFID}
          </Text>

          <InputWrapper>
            <TextInput
              mode="outlined"
              label="Peso (kg)"
              keyboardType="number-pad"
              style={{ flex: 1 }}
              value={weight}
              onChangeText={setWeight}
            />
            <TextInput
              mode="outlined"
              label="Nota Fiscal"
              keyboardType="number-pad"
              style={{ flex: 1, marginLeft: 6 }}
              value={invoice}
              onChangeText={setInvoice}
            />
          </InputWrapper>
          <InputWrapper>
            <TextInput
              mode="outlined"
              label="Número Pedido"
              style={{ flex: 1 }}
              keyboardType="number-pad"
              value={orderId}
              onChangeText={setOrderId}
            />
            <TextInput
              mode="outlined"
              style={{ flex: 1, marginLeft: 6 }}
              label="Destinatário"
              value={name}
              onChangeText={setName}
            />
          </InputWrapper>
          <TextInput
            mode="outlined"
            label="Endereço"
            value={address}
            onChangeText={setAddress}
          />
          <TextInput
            mode="outlined"
            label="Bairro"
            value={neighborhood}
            onChangeText={setNeighborhood}
          />
          <InputWrapper>
            <TextInput
              mode="outlined"
              style={{ flex: 1 }}
              label="Cidade"
              value={city}
              onChangeText={setCity}
            />
            <TextInput
              mode="outlined"
              style={{ flex: 1, marginLeft: 6 }}
              label="Estado"
              value={state}
              onChangeText={setState}
            />
          </InputWrapper>
          <InputWrapper>
            <TextInput
              mode="outlined"
              style={{ flex: 1 }}
              label="País"
              value={country}
              onChangeText={setCountry}
            />
            <TextInput
              mode="outlined"
              style={{ flex: 1, marginLeft: 6 }}
              label="Número"
              keyboardType="number-pad"
              value={number}
              onChangeText={setNumber}
            />
          </InputWrapper>

          <Text
            variant="titleSmall"
            style={{
              alignSelf: "center",
              textAlign: "center",
              marginTop: 12,
            }}
          >
            Insira a rota
          </Text>

          {routesArray?.map((route, i) => {
            return (
              <RouteWrapper key={i}>
                <PickerSelect
                  placeholder={{ value: null, label: "Selecione uma unidade" }}
                  onValueChange={(value) => (route.unitiesId = value)}
                  items={unities}
                  style={{
                    viewContainer: {
                      borderWidth: 1,
                      padding: 12,
                      borderColor: "rgb(128, 116, 124)",
                      borderRadius: 6,
                      marginBottom: 6,
                    },
                  }}
                />
                <Text variant="bodySmall" style={{ marginBottom: 6 }}>
                  Selecione a data de entrega prevista
                </Text>
                <DateTimePicker
                  mode="date"
                  value={route.estimateArriveDate}
                  timeZoneOffsetInMinutes={-3}
                  onChange={(date) => {
                    if (date.type !== "set") return;
                    route.estimateArriveDate = new Date(
                      date.nativeEvent.timestamp as number
                    );
                  }}
                />
              </RouteWrapper>
            );
          })}

          <ButtonTouchable onPress={addStep}>
            <Avatar.Icon
              icon="plus"
              size={32}
              style={{ marginTop: 12, alignSelf: "center" }}
            />
          </ButtonTouchable>

          <Button
            mode="contained"
            style={{ marginTop: 12 }}
            onPress={saveInfos}
          >
            Salvar
          </Button>
        </Scroll>
      </Modal>
    </Portal>
  );
}

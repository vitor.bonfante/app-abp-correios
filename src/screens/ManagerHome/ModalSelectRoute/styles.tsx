import styled from "styled-components/native";

export const RouteWrapper = styled.View`
  justify-content: center;
  align-items: center;
`;

export const ButtonTouchable = styled.TouchableOpacity``;

export const InputWrapper = styled.View`
  flex-direction: row;
`;

export const Scroll = styled.ScrollView``;

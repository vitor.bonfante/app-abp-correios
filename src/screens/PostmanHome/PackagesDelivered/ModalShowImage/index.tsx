import { Portal, Modal } from "react-native-paper";
import { Image } from "./styles";

interface Props {
  isOpen: boolean;
  setModalClose: () => void;
  photoURL: string;
}

export default function ModalShowImage({
  isOpen,
  setModalClose,
  photoURL,
}: Props) {
  return (
    <Portal>
      <Modal
        visible={isOpen}
        contentContainerStyle={{ backgroundColor: "white", padding: 20 }}
        onDismiss={setModalClose}
      >
        <Image source={{ uri: photoURL }} resizeMode="contain" />
      </Modal>
    </Portal>
  );
}

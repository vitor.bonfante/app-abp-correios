import { useState } from "react";
import { RefreshControl } from "react-native-gesture-handler";
import apiAuth from "../../../api/apiAuth";
import PackagesDeliveredComp from "../../../components/PackagesDelivered";
import ModalShowImage from "./ModalShowImage";
import { Container, FlatList } from "./styles";

export default function PackagesDelivered() {
  const [loading, setLoading] = useState<boolean>(false);
  const [packages, setPackages] = useState<any[]>([]);
  const [selectedPhoto, setSelectedPhoto] = useState<string>("");
  const [modalOpen, setModalOpen] = useState<boolean>(false);

  async function listPackages() {
    setLoading(true);
    await apiAuth
      .get("/package/list?isDelivered=true")
      .then((res) => {
        setPackages(res.data);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        return alert(err.response.data.ServerMessage.message);
      });
  }

  function openModal() {
    setModalOpen(true);
  }
  function closeModal() {
    setModalOpen(false);
    listPackages();
  }

  return (
    <Container>
      <ModalShowImage
        isOpen={modalOpen}
        photoURL={selectedPhoto}
        setModalClose={closeModal}
      />
      <FlatList
        data={packages}
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={listPackages} />
        }
        keyExtractor={(_, index) => index.toString()}
        renderItem={({ item }: any) => {
          return (
            <PackagesDeliveredComp
              date={new Date(item.PackageLogs[0].arriveDate)}
              rfid={item.rfid}
              onPress={() => {
                setSelectedPhoto(item.deliveredPhotoUrl);
                openModal();
              }}
            />
          );
        }}
      />
    </Container>
  );
}

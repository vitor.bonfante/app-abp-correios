import { useFocusEffect } from "@react-navigation/native";
import React from "react";
import { useState } from "react";
import { RefreshControl } from "react-native-gesture-handler";
import apiAuth from "../../../api/apiAuth";
import NoRegisters from "../../../components/NoRegisters";
import PackagesToDeliveryComp from "../../../components/PackagesToDelivery";
import ModalCamera from "./ModalCamera";
import { Container, FlatList, Scroll } from "./styles";

export default function PackagesToDelivery() {
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [packages, setPackages] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [packageId, setPackageId] = useState<string>("");

  function deliverPackage() {
    setModalOpen(true);
  }

  function closeModal() {
    setModalOpen(false);
    listPackages();
  }

  async function listPackages() {
    setLoading(true);
    await apiAuth
      .get("/package/list")
      .then((res) => {
        setPackages(res.data);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        return alert(err.response.data.ServerMessage.message);
      });
  }

  useFocusEffect(
    React.useCallback(() => {
      listPackages();
    }, [])
  );

  return (
    <Container>
      <ModalCamera
        isOpen={modalOpen}
        setModalClose={closeModal}
        packageId={packageId}
      />
      {packages.length > 0 ? (
        <FlatList
          data={packages}
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={listPackages} />
          }
          keyExtractor={(_, index) => index.toString()}
          renderItem={({ item }: any) => {
            const address = `${item.address.address} - Nº${item.address.number} - Bairro ${item.address.neighborhood}, ${item.address.city}/${item.address.state}/${item.address.country}`;
            return (
              <PackagesToDeliveryComp
                address={address}
                onPress={() => {
                  setPackageId(item.id);
                  deliverPackage();
                }}
              />
            );
          }}
        />
      ) : (
        <Scroll
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={listPackages} />
          }
        >
          <NoRegisters />
        </Scroll>
      )}
    </Container>
  );
}

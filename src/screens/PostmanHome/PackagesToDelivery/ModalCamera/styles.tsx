import styled from "styled-components/native";

export const ChangeCameraType = styled.TouchableOpacity`
  align-self: flex-end;
  margin: 12px;
`;

export const Loading = styled.View`
  position: absolute;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

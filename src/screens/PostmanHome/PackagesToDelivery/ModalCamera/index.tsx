import {
  Portal,
  Modal,
  Text,
  Button,
  Avatar,
  ActivityIndicator,
} from "react-native-paper";
import { Camera, CameraType } from "expo-camera";
import { useEffect, useState } from "react";
import { ChangeCameraType, Loading } from "./styles";
import apiAuth from "../../../../api/apiAuth";

interface Props {
  isOpen: boolean;
  setModalClose: () => void;
  packageId: string;
}

export default function ModalCamera({
  isOpen,
  setModalClose,
  packageId,
}: Props) {
  const [type, setType] = useState<CameraType>(CameraType.back);
  const [permission, requestPermission] = Camera.useCameraPermissions();
  const [loading, setLoading] = useState<boolean>(false);
  let camera: Camera;

  useEffect(() => {
    requestPermission();
  }, []);

  function changeCameraType() {
    setType((current) =>
      current === CameraType.back ? CameraType.front : CameraType.back
    );
  }

  async function confirmDelivery() {
    if (!camera) return;
    setLoading(true);

    camera.pausePreview();

    const photo = await camera.takePictureAsync({ quality: 0.1 });

    let localUri = photo.uri;
    let filename = localUri.split("/").pop();
    let match = /\.(\w+)$/.exec(filename!);
    let type = match ? `image/${match[1]}` : `image`;
    let formData = new FormData();
    //@ts-ignore
    formData.append("file", { uri: localUri, name: filename, type: type });

    await apiAuth
      .post("/upload/file/mobile", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(async (res) => {
        const URL = res.data.url;
        await apiAuth
          .put(`/package/deliver/${packageId}`, {
            imageUrl: URL,
          })
          .then((res) => {
            setLoading(false);
            setModalClose();
            alert("Pacote entregue com sucesso!");
          })
          .catch((err) => {
            setLoading(false);
            return alert(err.response.data.ServerMessage.message);
          });
      })
      .catch((err) => {
        setLoading(false);
        return alert(err.response.data.ServerMessage.message);
      });
  }

  return (
    <Portal>
      <Modal
        visible={isOpen}
        contentContainerStyle={{ backgroundColor: "white", padding: 20 }}
        onDismiss={setModalClose}
      >
        <Camera
          type={type}
          style={{ width: "100%", height: 400 }}
          ref={(r) => {
            camera = r as Camera;
          }}
        >
          <ChangeCameraType onPress={changeCameraType} disabled={loading}>
            <Avatar.Icon icon="camera-front" size={48} />
          </ChangeCameraType>
          {loading && (
            <Loading>
              <ActivityIndicator size={62} />
            </Loading>
          )}
        </Camera>
        <Button
          icon="camera-iris"
          mode="contained"
          style={{ marginTop: 12 }}
          onPress={confirmDelivery}
        >
          Confirmar entrega
        </Button>
      </Modal>
    </Portal>
  );
}

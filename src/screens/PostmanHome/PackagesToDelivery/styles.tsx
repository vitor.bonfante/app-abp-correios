import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
  padding: 0px 12px;
`;

export const FlatList = styled.FlatList``;

export const Scroll = styled.ScrollView``;

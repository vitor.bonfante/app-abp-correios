import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { RouteStackParamList } from "../../utils/RouteParamList";
import { NavigationContainer } from "@react-navigation/native";
import PackagesToDelivery from "./PackagesToDelivery";
import PackagesDelivered from "./PackagesDelivered";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const Tab = createBottomTabNavigator();

export default function PostmanHome({
  navigation,
}: RouteStackParamList<"PostmanHome">) {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarActiveTintColor: "#000",
        tabBarIcon: ({ focused }) => {
          switch (route.name) {
            case "PackagesToDelivery":
              return (
                <Icon
                  name="dolly"
                  size={30}
                  style={{ opacity: focused ? 1 : 0.3 }}
                />
              );
            case "PackagesDelivered":
              return (
                <Icon
                  name="truck-check"
                  size={30}
                  style={{ opacity: focused ? 1 : 0.3 }}
                />
              );
          }
        },
      })}
    >
      <Tab.Screen
        name="PackagesToDelivery"
        component={PackagesToDelivery}
        options={{
          title: "Pacotes para entrega",
          headerShown: false,
        }}
      />
      <Tab.Screen
        name="PackagesDelivered"
        component={PackagesDelivered}
        options={{
          title: "Pacotes entregues",
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
}

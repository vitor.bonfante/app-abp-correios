import AsyncStorage from "@react-native-async-storage/async-storage";
import React from "react";
import { Avatar, Button } from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";
import { RouteStackParamList } from "../../utils/RouteParamList";
import { Container } from "./styles";

export default function HomeScreen({
  navigation,
}: RouteStackParamList<"HomeScreen">) {
  async function clearStorage() {
    await AsyncStorage.clear();
  }

  return (
    <SafeAreaView style={{ backgroundColor: "#fff" }}>
      <Container>
        <Avatar.Icon
          icon="package-variant"
          size={82}
          style={{ alignSelf: "center", marginBottom: 48 }}
        />
        <Button
          icon="dolly"
          mode="contained"
          onPress={() => navigation.push("TrackPackage")}
        >
          Rastrear encomenda
        </Button>
        <Button
          style={{ marginTop: 24, marginBottom: 24 }}
          icon="account-group"
          mode="contained"
          onPress={() => navigation.push("Login")}
        >
          Login
        </Button>
      </Container>
    </SafeAreaView>
  );
}

import styled from "styled-components/native";

export const Container = styled.View`
  padding: 12px;
  height: 100%;
  display: flex;
  justify-content: center;
`;

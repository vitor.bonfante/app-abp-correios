import AsyncStorage from "@react-native-async-storage/async-storage";
import { useFocusEffect } from "@react-navigation/native";
import React from "react";
import { useState } from "react";
import { RefreshControl } from "react-native-gesture-handler";
import { Button, TextInput, Text } from "react-native-paper";
import apiPublic from "../../api/api";
import TrackingStep from "../../components/TrackingStep";
import {
  checkoutLogId,
  deliveredId,
  entryLogId,
  exitLogId,
  systemEntry,
} from "../../consts/PackageLogsType";
import { Container, HR, SafeArea, TrackContainer } from "./styles";

interface PackageInfo {
  invoice: string;
  orderId: string;
  name: string;
  weight: string;
  PackageLogs: [
    {
      arriveDate: Date;
      estimateArriveDate: Date;
      logTypes: {
        id: string;
        name: string;
      };
      unity: {
        name: string;
      };
      wasHere: boolean;
    }
  ];
}

export default function TrackPackage() {
  const [code, setCode] = useState<string>("");
  const [packageInfo, setPackageInfo] = useState<PackageInfo>();
  const [loading, setLoading] = useState<boolean>(false);

  async function track() {
    if (!code) {
      setLoading(false);
      return alert("Insira um código de rastreio");
    }
    setLoading(true);

    await apiPublic
      .get(`/package/info/${code}`)
      .then((res) => {
        setLoading(false);
        setPackageInfo(res.data);
        setCache(res.data);
      })
      .catch((err) => {
        setLoading(false);
        if (!err.response) {
          //caso servidor nao responda, puxa o cache (caso exista)
          getFromCache();
        } else {
          return alert(err.response.data.ServerMessage.message);
        }
      });
  }

  function chooseIcon(id: string) {
    switch (id) {
      case systemEntry:
        return "systemEntry";
      case entryLogId:
        return "received";
      case exitLogId:
        return "forwarded";
      case checkoutLogId:
        return "delivery_route";
      case deliveredId:
        return "delivered";
    }
  }

  async function setCache(packageInfo: PackageInfo) {
    const cache = await AsyncStorage.getItem("track_cache");
    if (!cache) {
      await AsyncStorage.setItem(
        "track_cache",
        JSON.stringify([{ code, packageInfo }])
      );
    } else {
      const cacheFormatted = JSON.parse(cache);
      cacheFormatted.push({ code, packageInfo });
      await AsyncStorage.setItem("track_cache", JSON.stringify(cacheFormatted));
    }
  }

  async function getFromCache() {
    const cache = await AsyncStorage.getItem("track_cache");
    if (!cache) return console.log("sem cache");

    const cacheParsed = JSON.parse(cache);
    const cacheCode = cacheParsed.find((c: any) => c.code === code);

    if (cacheCode) setPackageInfo(cacheCode.packageInfo);
  }

  return (
    <SafeArea>
      <Container>
        <TextInput
          style={{ margin: 12 }}
          label="Código de rastreio"
          value={code}
          onChangeText={setCode}
        />
        <Button style={{ margin: 12 }} mode="contained" onPress={track}>
          Buscar
        </Button>
        <HR />
        <TrackContainer
          refreshControl={
            <RefreshControl refreshing={loading} onRefresh={track} />
          }
        >
          {packageInfo && (
            <>
              <Text variant="titleMedium">
                Destinatário: {packageInfo.name}
              </Text>
              <Text variant="titleMedium">Peso: {packageInfo.weight} (KG)</Text>
              <Text variant="titleMedium">N Pedido: {packageInfo.orderId}</Text>
              <Text variant="titleMedium">N Fiscal: {packageInfo.invoice}</Text>
              <Text variant="titleMedium" style={{ marginBottom: 12 }}>
                Data de entrega prevista:{" "}
                {new Date(
                  packageInfo.PackageLogs.find(
                    (log) => log.logTypes.id === deliveredId
                  )?.estimateArriveDate as Date
                ).toLocaleDateString("pt-BR")}
              </Text>
              {packageInfo.PackageLogs.map((log, i) => {
                return (
                  log.wasHere && (
                    <TrackingStep
                      key={i}
                      step={chooseIcon(log.logTypes.id)}
                      status={`${log.logTypes.name} - ${log.unity.name}`}
                      date={new Date(log.arriveDate)}
                    />
                  )
                );
              })}
            </>
          )}
        </TrackContainer>
      </Container>
    </SafeArea>
  );
}

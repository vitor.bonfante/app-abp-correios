import styled from "styled-components/native";

export const SafeArea = styled.SafeAreaView`
  flex: 1;
  background-color: #fff;
`;

export const Container = styled.View``;

export const HR = styled.View`
  width: 100%;
  border: 1px solid rgb(167, 43, 162);
`;

export const TrackContainer = styled.ScrollView`
  padding: 12px;
  margin-bottom: 24px;
  height: 100%;
`;

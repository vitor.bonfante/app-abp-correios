import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Avatar,
  Button,
  Text,
  TextInput,
} from "react-native-paper";
import apiPublic from "../../api/api";
import apiAuth from "../../api/apiAuth";
import { RouteStackParamList } from "../../utils/RouteParamList";
import { Container, LoadingContainer, SafeArea } from "./styles";

enum UserTypes {
  MANAGER_TYPE = "Manager",
  POSTMAN_TYPE = "Postman",
}

export default function Login({ navigation }: RouteStackParamList<"Login">) {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    checkIfItsLogged();
  }, []);

  async function checkIfItsLogged() {
    const userType = await AsyncStorage.getItem("user_type");
    if (!userType) {
      setLoading(false);
      return;
    }
    switch (userType) {
      case UserTypes.MANAGER_TYPE:
        navigation.reset({
          routes: [{ name: "HomeScreen" }, { name: "ManagerHome" }],
        });
        return navigation.navigate("ManagerHome");
      case UserTypes.POSTMAN_TYPE:
        navigation.reset({
          routes: [{ name: "HomeScreen" }, { name: "PostmanHome" }],
        });
        return navigation.navigate("PostmanHome");
    }
  }

  async function handleLogin() {
    setLoading(true);
    await apiPublic
      .post("/user/login", {
        email,
        password,
      })
      .then(async (res) => {
        const token = res.data.accessToken;
        await AsyncStorage.setItem("access_token", token);
        await apiAuth
          .get("/auth/validate/token", {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          })
          .then(async (res) => {
            const userType = res.data.User.permissions[0].Permission.name;
            await AsyncStorage.setItem("user_type", userType);
            switch (userType) {
              case UserTypes.MANAGER_TYPE:
                navigation.reset({
                  routes: [{ name: "HomeScreen" }, { name: "ManagerHome" }],
                });
                return navigation.navigate("ManagerHome");
              case UserTypes.POSTMAN_TYPE:
                navigation.reset({
                  routes: [{ name: "HomeScreen" }, { name: "PostmanHome" }],
                });
                return navigation.navigate("PostmanHome");
            }
          });
      })
      .catch((err) => {
        setLoading(false);
        return alert(err.response.data.ServerMessage.message);
      });
  }

  return (
    <SafeArea>
      {loading && (
        <LoadingContainer>
          <ActivityIndicator size={52} />
        </LoadingContainer>
      )}
      <Container>
        <Avatar.Icon
          size={64}
          icon="account-group"
          style={{ alignSelf: "center", opacity: loading ? 0.5 : 1 }}
        />
        <Text
          variant="titleLarge"
          style={{
            alignSelf: "center",
            marginTop: 12,
            opacity: loading ? 0.5 : 1,
          }}
        >
          Login
        </Text>
        <TextInput
          mode="outlined"
          label="E-mail"
          style={{
            marginTop: 12,
            marginBottom: 12,
          }}
          disabled={loading}
          value={email}
          onChangeText={setEmail}
        />
        <TextInput
          mode="outlined"
          label="Senha"
          secureTextEntry
          disabled={loading}
          value={password}
          onChangeText={setPassword}
        />
        <Button
          onPress={handleLogin}
          disabled={loading}
          mode="contained"
          style={{ marginTop: 24 }}
        >
          LOGIN
        </Button>
      </Container>
    </SafeArea>
  );
}

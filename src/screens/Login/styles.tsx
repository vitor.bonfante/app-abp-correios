import styled from "styled-components/native";

export const SafeArea = styled.SafeAreaView`
  flex: 1;
  background-color: #fff;
`;

export const Container = styled.View`
  padding: 12px;
  height: 100%;
  display: flex;
  justify-content: center;
`;

export const LoadingContainer = styled.View`
  position: absolute;
  margin-top: 40%;
  align-self: center;
`;

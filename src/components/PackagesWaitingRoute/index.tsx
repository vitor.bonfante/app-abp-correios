import { GestureResponderEvent, TouchableOpacityProps } from "react-native";
import { Avatar, Text } from "react-native-paper";
import { Card } from "./styles";

interface Props {
  onPress: (event: GestureResponderEvent) => void;
  id: string;
}

export default function PackagesWaitingRoute({ id, onPress }: Props) {
  return (
    <Card activeOpacity={0.8} onPress={onPress}>
      <Avatar.Icon icon="map-marker-question" size={48} />
      <Text style={{ flex: 1, marginLeft: 12 }} variant="bodyLarge">
        Pacote ID{" "}
        <Text variant="titleMedium" style={{ fontWeight: "bold" }}>
          {id}
        </Text>{" "}
        esperando rota
      </Text>
    </Card>
  );
}

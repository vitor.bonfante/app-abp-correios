import { GestureResponderEvent } from "react-native";
import { Avatar, Text } from "react-native-paper";
import { Card } from "./styles";

interface Props {
  date: Date;
  onPress: (event: GestureResponderEvent) => void;
  rfid: string;
}

export default function PackagesDeliveredComp({ date, onPress, rfid }: Props) {
  return (
    <Card activeOpacity={0.8} onPress={onPress}>
      <Avatar.Icon icon="truck-check" size={48} />
      <Text style={{ flex: 1, marginLeft: 12 }} variant="bodyLarge">
        Pacote{" "}
        <Text variant="bodyLarge" style={{ fontWeight: "bold" }}>
          {rfid}
        </Text>{" "}
        entregue em {new Date(date).toLocaleDateString("pt-br")} -{" "}
        {new Date(date).toLocaleTimeString("pt-br")}
      </Text>
    </Card>
  );
}

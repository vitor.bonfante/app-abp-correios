import styled from "styled-components/native";

export const Card = styled.TouchableOpacity`
  border: 1px solid gray;
  padding: 8px;
  border-radius: 8px;
  flex-direction: row;
  align-items: center;
  margin-top: 12px;
`;

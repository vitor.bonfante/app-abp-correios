import { Avatar, Text } from "react-native-paper";
import { Container, TextContainer } from "./styles";

interface Props {
  step:
    | "systemEntry"
    | "received"
    | "forwarded"
    | "delivery_route"
    | "delivered"
    | undefined;
  status: string;
  date: Date;
}

const ICON_SIZE = 48;

export default function TrackingStep({ step, status, date }: Props) {
  function chooseIcon() {
    switch (step) {
      case "systemEntry":
        return "package-down";
      case "received":
        return "truck-check";
      case "forwarded":
        return "truck-fast";
      case "delivery_route":
        return "dolly";
      case "delivered":
        return "map-marker-check";
      default:
        return "truck-check";
    }
  }

  return (
    <Container>
      <Avatar.Icon size={ICON_SIZE} icon={chooseIcon()} />
      <TextContainer>
        <Text style={{ marginLeft: 12 }} variant="bodyLarge">
          {status}
        </Text>
        <Text style={{ marginLeft: 12 }} variant="bodySmall">
          {date.toLocaleDateString("pt-BR")} -{" "}
          {date.toLocaleTimeString("pt-BR")}
        </Text>
      </TextContainer>
    </Container>
  );
}

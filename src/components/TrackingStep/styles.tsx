import styled from "styled-components/native";

export const Container = styled.View`
  margin: 6px 0px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const TextContainer = styled.View``;

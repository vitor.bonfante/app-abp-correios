import { GestureResponderEvent, TouchableOpacityProps } from "react-native";
import { Avatar, Text } from "react-native-paper";
import { Card } from "./styles";

interface Props {
  onPress: (event: GestureResponderEvent) => void;
  address: string;
}

export default function PackagesToDeliveryComp({ onPress, address }: Props) {
  return (
    <Card activeOpacity={0.8} onPress={onPress}>
      <Avatar.Icon icon="dolly" size={48} />
      <Text style={{ flex: 1, marginLeft: 12 }} variant="bodyLarge">
        {address}
      </Text>
    </Card>
  );
}

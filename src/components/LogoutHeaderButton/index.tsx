import { TouchableOpacityProps } from "react-native";
import { Button } from "./styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default function LogoutHeaderButton(props: TouchableOpacityProps) {
  return (
    <Button {...props} activeOpacity={0.5}>
      <Icon name="logout" size={24} />
    </Button>
  );
}

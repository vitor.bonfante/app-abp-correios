import { View } from "react-native";
import { Avatar, Text } from "react-native-paper";

export default function NoRegisters() {
  return (
    <View
      style={{ alignSelf: "center", marginTop: "50%", alignItems: "center" }}
    >
      <Avatar.Icon icon="emoticon-sad" size={48} />
      <Text variant="titleLarge" style={{ marginTop: 12 }}>
        Sem registros{" "}
      </Text>
    </View>
  );
}

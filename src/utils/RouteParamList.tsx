import { RouteProp } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";

export type RouteParamList = {
  HomeScreen: undefined;
  TrackPackage: undefined;
  Login: undefined;
  PostmanHome: undefined;
  ManagerHome: undefined;
};

export type RouteStackParamList<T extends keyof RouteParamList> = {
  navigation: NativeStackNavigationProp<RouteParamList, T>;
  route: RouteProp<RouteParamList, T>;
};
